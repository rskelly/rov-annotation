## Extract image and polygon areas in square meters
# Dataset was created with 4.merge-reports.R

# Extract field of view area from attribute column
dat$image_area_sqm <- sub(".*\"area\":", "", dat$attributes)
dat$image_area_sqm <- as.numeric(substr(x=dat$image_area_sqm, 1, 8))

# Check for image field of view area errors
# Images with error are likely missing laser point annotations
unique(dat$filename[grep("\"error\":true",dat$attributes)])

# Check for NA values
summary(dat$image_area_sqm)

# Check polygon areas in sqm and sqpx units
# Look for differences in the number of missing values
summary(dat$annotation_area_sqm)
summary(dat$annotation_area_sqpx)

# If the square meter column is missing more values,
# calculate sqm from sqpx (pixels) using the image square meter area
# Image height and width in pixels is found in the attribute column
image_pixel_area <- 1920 * 1080
percent_area <- dat$annotation_area_sqpx / image_pixel_area
dat$annotation_area_sqm <- percent_area * dat$image_area_sqm

# check for images with polygons have don't have areas
dat$filename[which(is.na(dat$annotation_area_sqm) &
                     dat$shape_name != "Point")]
