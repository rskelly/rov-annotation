###################################################################
# Batch Image Capture Tool
#
# Originally developed by Jonathan Martin (ctenophore@gmail.com)
# January 2019
# Adapted by Jessica Nephin (jessica.nephin@dfo-mpo.gc.ca)
# March 2019
#
# Requirements:
#   Requires ffmpeg command line tool to capture images,
#   Download at https://ffmpeg.org/
#
# Description:
#   1) Captures images at a specified frequency from videos listed
#      in an imported csv file.
#   2) Renames the images by transect name, date and time for
#      uploading to Biigle.
#
# Instructions:
#   1) Check that all required modules are installed.
#   2) Modify inputs (e.g. rate of image capture).
#   3) Run python script from the directory containing the videos.
###################################################################


###################################################################
#   Required modules
###################################################################

import os
from datetime import datetime
from datetime import timedelta
import subprocess
import csv
import re


###################################################################
#   Inputs
###################################################################

# Rate of image capture in seconds
stillsFrequency = 5

# Full path to csv with video filenames,
# file should contain video filename, start time and transect name,
# without headings
# column 1 = Filename (e.g. videofile.mov)
# column 2 = Start datetime (e.g. yyyy/mm/dd hh:mm:ss)
# column 3 = Transect name
videofiles = "<path to directory>/<name of file>.csv"

# Directory to save images
imageDir = "<path to directory>/<name of directory for images>/"


###################################################################
#   Setup
###################################################################

# Function to convert a csv file into dictionaries
def csv_dict(variables_file, col):
    # Load csv and create a dictionary
    with open(variables_file, mode='r') as infile:
        reader = csv.reader(infile)
        dict_list = {rows[0]:rows[col] for rows in reader}
        return dict_list

# Calls csv_dict,
# returns dictionary of video filenames and start times
startsDict = csv_dict(videofiles, 1)
transectsDict = csv_dict(videofiles, 2)

# Get video filenames
videoNames = [k  for  k in  startsDict]


###################################################################
#   Loop through each video file and capture images
###################################################################

# Loop through each video file
for video in videoNames:

    # Get actualStartTime
    actualStartTime = startsDict[video]

    # Get transect name
    transectName = transectsDict[video]

    # Create directory to save images if it doesn't exist
    if not os.path.exists(imageDir + transectName):
        os.makedirs(imageDir + transectName)

    # rename all image files with timestamps
    def renameImageFiles():
        global actualStartTime
        global stillsFrequency
        counter = 0

        #stillTime
        startTimeDateTime = datetime.strptime(
                actualStartTime, '%Y-%m-%d %H:%M:%S')

        # files from image grabs
        imageList = os.listdir(imageDir + transectName)

        for fileName in imageList:

            # Elapsed seconds
            elapsedSecs = stillsFrequency * counter
            # Calculate elapsed time
            elapsedDatetime = (startTimeDateTime +
                               timedelta(seconds=elapsedSecs))
            # text string
            strDateTime = str(elapsedDatetime)
            # sub space for underscore
            nospaces = re.sub("\s+", "_", strDateTime)
            nocolons = re.sub(":", "", nospaces)
            fileDateTime = (transectName + "_" +
                            re.sub("-", "",nocolons))

            # rename
            orig = imageDir + transectName + "/" + fileName
            new = (imageDir + transectName + "/" +
                   fileDateTime + ".jpeg")
            os.rename(orig, new)

            # add to counter
            counter += 1

    # Get framerate of video
    command = ("ffprobe -v error -select_streams v -of "
               "default=noprint_wrappers=1:nokey=1 "
               "-show_entries stream=r_frame_rate " + video)
    FrameRate = str( subprocess.check_output(command) )
    fr = FrameRate.split("/")
    frNum = float(fr[0]) / float(fr[1])

    # Image capture frequency calculated with framerate
    FrameRateFreq = str(int(round( frNum * stillsFrequency )))

    # generate stills, start at the beginning of video
    path = imageDir +"/"+ transectName +"/"+ transectName
    FFMCommand = ("ffmpeg -ss 0 -i " + video +
                  " -vf \"select=not(mod(n\," +
                  FrameRateFreq + "))\" -vsync vfr -q:v 1 " +
                  path + "_%03d.jpg")
    subprocess.call(FFMCommand, shell = True)

    #adds time stamp to captures and moves them to a named directory
    renameImageFiles()
