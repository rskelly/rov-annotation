# Full path to the folder containing images or videos for BIIGLE volume
media_folder <- "D:/Surveys/Pac2017-030_VectorSept/ImageGrabs/HC3"

# Get list of files in media_folder
list_filenames <- list.files(path=media_folder)

# Separate by comma
comma_filenames <- paste(list_filenames, collapse = ",")

# Copy output and paste into BIIGLE
comma_filenames
