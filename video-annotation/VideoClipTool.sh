# Example code and instructions for how to create video clips using ffmpeg
# Requirements:
#   Requires ffmpeg command line tool,
#   Download at https://ffmpeg.org/

# For this example the datetime the original video started recording is
# DDMMYYY_HHMMSS = 03102020_140420

# 1) Find the elapsed time when the transect begins
# In this example it is 2 minutes in
# -ss and -ss before and after -i should add up to 2min in this example
# the first -ss should be a minute less of the time you are looking for
# the second -ss should always be a minute
# having two -ss speeds this up
# 2) Set -t to be 00:10:00 for 10 minute video clips
# 3) To get the correct HHMMSS for the filename add both -ss values to the start time of the original video file
ffmpeg -ss 00:01:00 -i original.mpg -ss 00:01:00 -t 00:10:00 -c copy clip_03102020_140620.mp4
# 4) For the next clip add -ss before -i from the previous clip and to the -t values to get your start
# the -ss after -i and the -t always stays the same, then add 5 seconds to that (so there is no overlap)
# Same as with the first clip add both -ss values to the original start time to get the time for the filename
# e.g. 14:04:20 + 00:11:05 + 00:01:00 = 14:16:25
ffmpeg -ss 00:11:05 -i original.mpg -ss 00:01:00 -t 00:10:00 -c copy clip_03102020_141625.mp4
# 5) Repeate steps 3) and 4) until you get to the end of the video
